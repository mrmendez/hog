**Programas y agoritmos sobre temas de matemáticas aplicadas en la ingeniería en tecnologías de la información**

En este repositorio encontraras dentro de cada carpeta un tema de interés a la aplicación de las matemáticas en el campo de la ingeniería en tecnologías de la información.

---

## Histograma de gradientes orientados

En este programa podrás ver herramienta matemática utilizada en el campo de visión por computadora.

1. Uso del **gradiente**
2. Concepto del vector como gradiente para obtención de magnitud y ángulo.
3. Concepto de un descriptor de extracción de características de una imagen.
4. Uso de un lenguaje de programación para su implementación.

---
